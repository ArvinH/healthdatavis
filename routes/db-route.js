var express = require('express');
var db = require('../public/javascripts/db');
var router = express.Router();


router.post('/query', function(req, res, next) {
	var reason = req.body.reason,
	    gender = req.body.gender,
	    year = req.body.year
	var queryString;
	    //queryString = "select 縣市, 死亡率 from countiesandage where 西元年='"+year+"' and 死因別='"+reason+"' and 性別='"+gender+"' and 年齡別='All age'"
	    queryString = "select 縣市, 死亡率,信賴區間,orders,誤差值 from category113 where 西元年='"+year+"' and 死因別='"+reason+"' and 性別='0' and 年齡別='All age' and 縣市!='連江縣' order by orders"
		
	//var queryString = "select * from age where 死因別='自殺' and 年齡別='<=14' and 性別='1'";
	db.query(queryString, function(dbresult){
		console.log(queryString);
		res.setHeader('Content=Type','applicatioin/json');
		res.json(dbresult);
	});
	
});

router.get('/initial',function(req, res, next){
	  db.query('select 西元年 from yearOrder Order by orders',function(data1){
			  db.query('select 死因別 from reasonorder Order by orders',function(data2){
				 res.setHeader('Content-Type', 'application/json');
				 res.json([data1,data2]);
			  });
	  });
});

router.get('/bubble-initial',function(req, res, next){
	  db.query('select distinct 西元年 from countiesandage order by 西元年', function(data1){
		//db.query('select 死因別 from reasonorder Order by orders',function(data2){
		db.query('select distinct 死因別 from countiesandage',function(data2){
				 res.setHeader('Content-Type', 'application/json');
				 res.json([data1,data2]);
			  });
	});
});

router.post('/bubble-query/:id', function(req, res, next){

	if (req.params.id == 'bar'){
      var reason = req.body.reason,
	    year = req.body.year,
		city = req.body.city
	  var queryString;
	    queryString = "select 性別, 年齡別, 縣市, sum(死亡率) as 死亡率 from countiesandage where 西元年='"+year+"' and 性別!='0' and 死因別='"+reason+"' and 縣市='"+city+"' group by 性別 , 年齡別, 縣市";
		
		db.query(queryString, function(dbresult){
			console.log(queryString);
			res.setHeader('Content=Type','applicatioin/json');
			res.json(dbresult);
		});	  
	}
	else if (req.params.id == 'map'){
	  var reason = req.body.reason,
	    year = req.body.year
	  var queryString;
		queryString = "select 縣市, sum(死亡率) as 死亡率 from countiesandage where 西元年='"+year+"' and 死因別='"+reason+"' and 性別!='0' and 縣市!='連江縣' group by 縣市";
		
		db.query(queryString, function(dbresult){
			console.log(queryString);
			res.setHeader('Content=Type','applicatioin/json');
			res.json(dbresult);
		});

	}
});

module.exports = router;
