var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/bubble', function(req, res, next) {
  res.render('bubble', { title: 'Express' });
});

router.get('/deadrate', function(req, res, next) {
  res.render('deadrate', {title: 'Express'});
});

router.get('/story-tell', function(req, res, next) {
  res.render('story-tell', {title: 'Express'});
});

module.exports = router;
