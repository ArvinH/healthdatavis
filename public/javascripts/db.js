var pg = require('pg');

exports.query = function(queryString, callback){
    //var conString = "postgres://arvin:netdb@140.116.247.114:5432/healthdata";
    var conString = "postgres://arvin:netdb@localhost:5432/healthdata";
    
    var Results = [];
    pg.connect(conString, function(err, client, done) {

    var query = client.query(queryString);
      query.on('row', function(row) {
          Results.push(row);
      });

      // After all data is returned, close connection and return results
      query.on('end', function() {
          client.end();
          callback && callback.call( this, Results);
      });

      if(err) {
        return console.error('error running query', err);
      }

  });

};





