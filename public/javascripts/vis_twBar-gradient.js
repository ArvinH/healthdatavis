var tip2 = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([-10, 800])
	  .html(function(d) {
		return "<strong>" + d['縣市'] + "<br>死亡率:</strong> <span style='color:red'>" + d['死亡率']+' '+d['信賴區間'] + "</span>";
	  });
function initialBar(Data){
	var dataset = Data;
	var w = $(window).width()/2, h = 600,padding = 30, barMargin = 2;
		//定義寬高,padding等等值
	
	var Xmax = d3.max(dataset, function(d){return +d['死亡率']}),
	Xmin = d3.min(dataset, function(d){return +d['死亡率']});
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, dataset.length]) //傳入的值是原始資料的最小及最大值
		.range([padding, h-padding]);

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([padding-23, w - padding]);
	
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);

	var barHeight = (h - padding * 5) / dataset.length - barMargin;
	var errorValueHeight = barHeight/2;
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = dataset.length;
	window.padding = padding;
	window.h = h;
	window.w = w;
	window.barHeight = barHeight;
	var svg = d3.select('#barChart').append('svg').attr({'width': w+70,'height': h})
	
	svg.append('g').selectAll('rect').data(dataset).enter() 
	.append('rect')
	.on('mouseover', function(d){
		 var city = '.'+d['縣市'];
		 if (city == '.台灣'){
			$('.citymap').attr("opacity","0.5");
			tip2.show(d);
			$(this).attr("fill",'red');
		 }
		 else{
		 var event = document.createEvent("HTMLEvents");
		 event.initEvent("mouseover",true,false);
		 document.getElementsByClassName(d['縣市'])[0].dispatchEvent(event);	
		 $(this).attr("fill","red");
		}
	})
	.on('mouseout', function(d){
		 var city = '.'+d['縣市'];
		 if(city == '.台灣'){
			$('.citymap').attr("opacity","1");
			tip2.hide(d);
			$(this).attr("fill",$(this).attr("data-color"));
		 }
		 else {
		 var event = document.createEvent("HTMLEvents");
		 event.initEvent("mouseout",true,false);
		 document.getElementsByClassName(d['縣市'])[0].dispatchEvent(event);
	 	 $(this).attr("fill",$(this).attr("data-color")); 
		 }
		})
	.attr({	
		'class': 'bar',
		'id': function(d){ return d['縣市']+'_b';},
		'y': function(d, i){return yScale(i)+15}, 
		'x': function(d){return 55}, 
		'height': barHeight,
		'width':function(d){return xScale(d['死亡率'])}, 
		'fill': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'data-color': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'title': function(d){
			return 'Name : ' + d['縣市'];
		}
	}).call(tip2);

	svg.append('g').selectAll('rect').data(dataset).enter()
	.append('rect')
	.attr({
		'class':'errorValue',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+25},
		'x': function(d){
			if( d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + xScale(d['信賴區間'].split('-')[0].replace('(',''));
			}
		},
		'width': function(d){return xScale(2*d['誤差值'] || 0)},
		'height': errorValueHeight/3,
		'fill': 'SlateGray'
	});
	svg.append('g').selectAll('rect').data(dataset).enter()
	.append('rect')
	.attr({
		'class':'errorValue-start',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+22},
		'x': function(d){
			if( d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))-2.5);
			}
		},
		'width': function(d){return 3},
		'height': errorValueHeight,
		'fill': 'SlateGray'
	});
	svg.append('g').selectAll('rect').data(dataset).enter()
	.append('rect')
	.attr({
		'class':'errorValue-end',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+22},
		'x': function(d){
			if (55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))+xScale(2*d['誤差值']||0)-2.5) > $('#barChart svg').width()){
				return $('#barChart svg').width() - 3;	
			}
			else if (d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))+xScale(2*d['誤差值']||0)-2.5)
			}
		},
		'width': function(d){return 3},
		'height': errorValueHeight,
		'fill': 'SlateGray'
	});
	var xAxis = d3.svg.axis().scale(xScale) //增加軸線物件，並套用尺度(x)
	.orient("bottom") //標示位置
	.ticks(10); //刻度數量

	/*
	svg.append('g').selectAll('text').data(dataset).enter() //補上資料數值
	.append('text') 
	.text(function(d){ return d.population}) //將值寫到SVG上
	.attr({
		'class': 'data_text',
		'y': function(d, i){return yScale(i) + barHeight/2}, //和上面相同，算出X、Y的位置
		'x': function(d){return xScale(d.population) +70},
		//'fill': 'rgb(219, 58, 76)', 
		'fill': 'black', 
		'text-anchor': 'middle',
		'font-size': '10px' 
	});
	*/

	svg.append('g').selectAll('text').data(dataset).enter() 
	.append('text') 
	.text(function(d){ return d['縣市']}) 
	.attr({
		'class': 'city',
		'y': function(d, i){return yScale(i) + barHeight/2+15}, //和上面相同，算出X、Y的位置
		'x': function(d){return 30},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	});
	svg.append("text")      // text label for the x axis
	.attr("class","axis_text")
        .attr("x", 10 )
        .attr("y", padding - 10)
	.attr("fill","SlateGray")
        .text("(死亡率)");

	svg.append('g').attr('class', 'axis') //增加一個群組並加上class="axis"
	.attr('transform', 'translate(55, 25)') //移動到下方
	.call(xAxis); //將軸線匯入

}

function updateBar(data){

	var w = $(window).width()/2,h = 600,padding = 30, barMargin = 2;
		//定義寬高,padding等等值

	var Xmax = d3.max(data, function(d){return (+d['死亡數'] || +d['死亡率'] || 0)}),
	Xmin = d3.min(data, function(d){return (+d['死亡數'] || +d['死亡率'] || 0)});
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, data.length]) //傳入的值是原始資料的最小及最大值
		.range([padding, h - padding]); 
		//輸出的範圍是左邊的padd距離，到右邊的padding

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([padding-23, w - padding]);

	/*
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmax])
		.range([500, 0])
		//這次顏色都要用尺度來算
	*/
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);

		//.range(['#ffff56','#ffa500','#ff4d4d','#ff0000']);
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = data.length;
	var barHeight = (h -padding *5) / data.length - barMargin;
	var errorValueHeight = barHeight/2;
	var svg = d3.select('#barChart');
	var delay = function(d, i) { return i * 50; };
	svg.selectAll('g .bar').data(data)
        .on('mouseover', function(d){
		 var city = '.'+d['縣市'];
		 if (city == '.台灣'){
			$('.citymap').attr("opacity","0.5");
			tip2.show(d);
			$(this).attr("fill",'red');
		 }
		 else{
		 var event = document.createEvent("HTMLEvents");
		 event.initEvent("mouseover",true,false);
		 document.getElementsByClassName(d['縣市'])[0].dispatchEvent(event);	
		 $(this).attr("fill","red");
		}
	})
	.on('mouseout', function(d){
		 var city = '.'+d['縣市'];
		 if(city == '.台灣'){
			$('.citymap').attr("opacity","1");
			tip2.hide(d);
			$(this).attr("fill",$(this).attr("data-color"));
		 }
		 else {
		 var event = document.createEvent("HTMLEvents");
		 event.initEvent("mouseout",true,false);
		 document.getElementsByClassName(d['縣市'])[0].dispatchEvent(event);
	 	 $(this).attr("fill",$(this).attr("data-color")); 
		 }
		})
			   .transition().duration(750)
        		   .delay(delay)
			   .attr({	
				'id': function(d){ return d['縣市']+'_b';},
				'y': function(d, i){return yScale(i)+15}, 
				'x': function(d){return 55}, 
				'height': barHeight,
				'width':function(d){
					return xScale((+d['死亡數'] || +d['死亡率'] || 0));
				}, 
				'fill': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color); //利用尺度來算出顏色
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'data-color': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color);
					//return Map[d['縣市']]((+d['死亡數'] || +d['死亡率'] || 0));
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'title': function(d){
					return 'Name : ' + d['縣市'];
				}
			});
	svg.selectAll('g .errorValue').data(data)
	.transition().duration(750)
        .delay(delay)
	.attr({
		'class':'errorValue',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+25},
		'x': function(d){
			if( d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + xScale(d['信賴區間'].split('-')[0].replace('(',''));
			}
		},
		'width': function(d){return xScale(2*d['誤差值']||0)},
		'height': errorValueHeight/3,
		'fill': 'SlateGray'
	});
	svg.selectAll('g .errorValue-start').data(data)
	.transition().duration(750)
	.delay(delay)
	.attr({
		'class':'errorValue-start',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+22},
		'x': function(d){
			if(d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))-2.5)
			}
		},
		'width': function(d){return 3},
		'height': errorValueHeight,
		'fill': 'SlateGray'
	});
	svg.selectAll('g .errorValue-end').data(data)
	.transition().duration(750)
	.delay(delay)
	.attr({
		'class':'errorValue-end',
		'id': function(d){return d['縣市']+'_ev';},
		'y': function(d,i){return yScale(i)+22},
		'x': function(d){
			if (55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))+xScale(2*d['誤差值']||0)-2.5) > $('#barChart svg').width()){
				return $('#barChart svg').width() - 3;	
			}
			else if (d['信賴區間'] == '(NA)'){
				return 55;
			}
			else{
				return 55 + (xScale(d['信賴區間'].split('-')[0].replace('(',''))+xScale(2*d['誤差值']||0)-2.5)
			}
		},
		'width': function(d){return 3},
		'height': errorValueHeight,
		'fill': 'SlateGray'
	});
	var xAxis = d3.svg.axis().scale(xScale) //增加軸線物件，並套用尺度(x)
	.orient("bottom") //標示位置
	.ticks(10); //刻度數量

	/*
	svg.selectAll('.data_text').data(data).transition().duration(750)
        .delay(delay)
	.text(function(d){ return (d['死亡數'] || d['死亡率'] || 0)}) //將值寫到SVG上
	.attr({
		'y': function(d, i){return yScale(i) + barHeight/2}, //和上面相同，算出X、Y的位置
		'x': function(d){return xScale((+d['死亡數'] || +d['死亡率'] || 0)) +70},
		'fill': 'black', 
		'text-anchor': 'middle',
		'font-size': '10px'
	});
	*/

	svg.selectAll('.city').data(data).transition().duration(750)
        .delay(delay) 
	.text(function(d){ return d['縣市']}) 
	.attr({
		'y': function(d, i){return yScale(i) + barHeight/2+15}, //和上面相同，算出X、Y的位置
		'x': function(d){return 30},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	});
	
 	var axisLabel;
	if(data[0]['死亡數'] !== undefined){axisLabel="(死亡數)"}	
	else if(data[0]['死亡率']!== undefined){axisLabel="(死亡率)"}	

	svg.select(".axis_text").transition().duration(750)
        .delay(delay)     // text label for the x axis
        .text(axisLabel)
        .attr("x", 10 )
        .attr("y", padding - 10)
	.attr("fill","SlateGray");

	svg.selectAll('.axis') //增加一個群組並加上class="axis"
	.call(xAxis); //將軸線匯入
}

function sortBar(method){
	var xScale = d3.scale.linear()
		.domain([window.Xmin, window.Xmax])
		.range([window.padding-20, window.w - window.padding])

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, window.dataLength]) //傳入的值是原始資料的最小及最大值
		.range([window.padding, window.h - window.padding]) 
		//輸出的範圍是左邊的padd距離，到右邊的padding

	var sortItems;
	if(method == '0'){
	sortItems = function (a, b) {
       	  	return d3.descending((a['死亡數'] || a['死亡率'] || 0) , (b['死亡數'] || b['死亡率'] || 0));
   		};
	}
	else if(method == '1'){
	sortItems = function (a, b) {
       	  	return d3.ascending((a['orders'] ) , (b['orders']));
   		};
	}
	var svg = d3.select('#barChart');
	var delay = function(d, i) { return i * 50; };
    svg.selectAll("g .bar")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+15;
    });	
    svg.selectAll("g .errorValue")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+25;
    });
    svg.selectAll("g .errorValue-start")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+22;
    });
    svg.selectAll("g .errorValue-end")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+22;
    });
    svg.selectAll('.city').sort(sortItems).transition().duration(750)
        .delay(delay) 
	.text(function(d){ return d['縣市']}) 
	.attr({
		'y': function(d, i){return yScale(i) + window.barHeight/2+15}, //和上面相同，算出X、Y的位置
		'x': function(d){return 30},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	}); 
    /*
    svg.selectAll('.data_text').sort(sortItems).transition().duration(750)
        .delay(delay)
	.text(function(d){ return (d['死亡數'] || d['死亡率'] || 0)}) //將值寫到SVG上
	.attr({
		'y': function(d, i){return yScale(i) + window.barHeight/2}, //和上面相同，算出X、Y的位置
		'x': function(d){ return xScale((+d['死亡數'] || +d['死亡率'] || 0)) +70;},
		'fill': 'black', 
		'text-anchor': 'middle',
		'font-size': '10px'
	});
   */

}
