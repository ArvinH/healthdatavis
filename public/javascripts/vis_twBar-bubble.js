(function(){
	$.ajax({type:'POST', url:'/data/bubble-query/bar', data: {reason:'所有死因', year:'1981', city:'臺北市'},success: function(Data){
		initialBar_female(Data);
		initialBar_male(Data);
	}});	
})();

function divideData(Data, n){
	var len = Data.length, newData = [], i = 0;
    while (i < len) {
        var size = Math.ceil((len - i) / n--);
        newData.push(Data.slice(i, i + size));
        i += size;
    }
    return newData;
}
var tip = d3.tip()
	  .attr('class', 'd3-tip2')
	  .offset([65, 90])
	  .html(function(d) {
		return "<strong>死亡率:</strong> <span style='color:red'>" + d['死亡率'] + "</span>";
});

function cal_DeathRateTotal(Data){
	var total = 0;
	for( var i =0; i< Data.length; i++){
		total += Data[i]['死亡率'];	
	}
	return Math.round(total*100)/100;
}

function initialBar_female(Data){
	
	var dataset = divideData(Data,2)[0];
	var w = $('.gender-section').width()/2-90, h = $('.gender-section').height()*(3/4), padding = 30, barMargin = 2;
		//定義寬高,padding等等值
	$('.female_num').animate({opacity: "0"},500,function(){
		$(this)
		.hide()
		.text(cal_DeathRateTotal(dataset))
		.show().animate({opacity: "100"}, 1000);
	});
	var Xmax = d3.max(dataset, function(d){return +d['死亡率']}),
	Xmin = d3.min(dataset, function(d){return +d['死亡率']});
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, dataset.length]) //傳入的值是原始資料的最小及最大值
		.range([0, h/2]);

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([20, w - 2*padding]);
	
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#ff5cbd','#ff42b3','#ff29a8','#ff0f9e','#f50092']);
	
	var imgScale = d3.scale.linear()
		.domain([0,cal_DeathRateTotal(Data)])
		.range(['25px','55px']);
	$('#female_icon').css('width',imgScale(cal_DeathRateTotal(dataset)));

	var barHeight = (h - padding * 6) / (dataset.length*4) - barMargin;
	var errorValueHeight = barHeight/2;
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = dataset.length;
	window.padding = padding;
	window.h = h;
	window.w = w;
	window.barHeight = barHeight;
	var svg = d3.select('.tw_bar_female').append('svg').attr({'width': w+70,'height': h*(3/5)})
	$('.tw_bar_female svg').css({ 'position':'relative'});
	var locks = svg.append('g').selectAll('rect').data(dataset).enter() 
	.append('rect')
	.on('mouseover', function(d){
		tip.show(d);
	})

	.on('mouseout', function(d){
		tip.hide(d);
    })
	.attr({	
		'class': 'bar',
		'id': function(d){ return d['年齡別']+'_b';},
		'y': function(d, i){return yScale(i)+60}, 
		'x': function(d){return 50}, 
		'height': barHeight,
		'width':function(d){return xScale(d['死亡率'])}, 
		'fill': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'data-color': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'title': function(d){
			return 'Name : ' + d['年齡別'];
		}
	});
	locks.call(tip);
	
	var xAxis = d3.svg.axis().scale(xScale) //增加軸線物件，並套用尺度(x)
	.orient("bottom") //標示位置
	.ticks(10); //刻度數量

	/*
	svg.append('g').selectAll('text').data(dataset).enter() //補上資料數值
	.append('text') 
	.text(function(d){ return d.population}) //將值寫到SVG上
	.attr({
		'class': 'data_text',
		'y': function(d, i){return yScale(i) + barHeight/2}, //和上面相同，算出X、Y的位置
		'x': function(d){return xScale(d.population) +70},
		//'fill': 'rgb(219, 58, 76)', 
		'fill': 'black', 
		'text-anchor': 'middle',
		'font-size': '10px' 
	});
	*/

	svg.append('g').selectAll('text').data(dataset).enter() 
	.append('text') 
	.text(function(d){ return d['年齡別']}) 
	.attr({
		'class': 'age',
		'y': function(d, i){return yScale(i) + barHeight/2+65}, //和上面相同，算出X、Y的位置
		'x': function(d){return 20},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	});
	svg.append("text")      // text label for the x axis
	.attr("class","axis_text")
        .attr("x", 0 )
        .attr("y", padding+15)
	.attr("fill","SlateGray")
        .text("年齡別");
	/*
	svg.append('g').attr('class', 'axis') //增加一個群組並加上class="axis"
	.attr('transform', 'translate(55, 25)') //移動到下方
	.call(xAxis); //將軸線匯入
	*/

}
function initialBar_male(Data){
	
	var dataset = divideData(Data,2)[1];
	var w = $('.gender-section').width()/2-140, h = $('.gender-section').height()*(3/4), padding = 30, barMargin = 2;
		//定義寬高,padding等等值
	
	$('.male_num').animate({opacity: "0"},500,function(){
		$(this)
		.hide()
		.text(cal_DeathRateTotal(dataset))
		.show().animate({opacity: "100"}, 1000);
	});
	var Xmax = d3.max(dataset, function(d){return +d['死亡率']}),
	Xmin = d3.min(dataset, function(d){return +d['死亡率']});
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, dataset.length]) //傳入的值是原始資料的最小及最大值
		.range([0, h/2]);

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([20, w - padding]);
	
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#80e2e2','#6cdddd','#57d9d9','#42d4d4','#2fcece']);

	var imgScale = d3.scale.linear()
		.domain([0,cal_DeathRateTotal(Data)])
		.range(['25px','55px']);
	$('#male_icon').css('width',imgScale(cal_DeathRateTotal(dataset)));

	var barHeight = (h - padding * 6) / (dataset.length*4) - barMargin;
	var errorValueHeight = barHeight/2;
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = dataset.length;
	window.padding = padding;
	window.h = h;
	window.w = w;
	window.barHeight = barHeight;
	var svg = d3.select('.tw_bar_male').append('svg').attr({'width': w+70,'height': h*(3/5)})
	$('.tw_bar_male svg').css({ 'position':'relative'});
	svg.append('g').selectAll('rect').data(dataset).enter() 
	.append('rect')
	.on('mouseover', function(d){
		tip.show(d);
	})

	.on('mouseout', function(d){
		tip.hide(d);
    })
	.attr({	
		'class': 'bar',
		'id': function(d){ return d['年齡別']+'_b';},
		'y': function(d, i){return yScale(i)+60}, 
		'x': function(d){return w+60-xScale(d['死亡率'])}, 
		'height': barHeight,
		'width':function(d){return xScale(d['死亡率'])}, 
		'fill': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'data-color': function(d){
			return colorScale(d['死亡率']);
			//return Map[d.city](d.population);
		},
		'title': function(d){
			return 'Name : ' + d['年齡別'];
		}
	}).call(tip);

}

function updateBar_female(data, totalDeath){
	var w = $('.gender-section').width()/2-90, h = $('.gender-section').height()*(3/4), padding = 30, barMargin = 2;
		//定義寬高,padding等等值

	$('.female_num').text(cal_DeathRateTotal(data))
	var Xmax = d3.max(data, function(d){return  +d['死亡率']}),
	Xmin = d3.min(data, function(d){return +d['死亡率'] });
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, data.length]) //傳入的值是原始資料的最小及最大值
		.range([0, h/2]); 
		//輸出的範圍是左邊的padd距離，到右邊的padding

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([20, w - 2*padding]);

	var imgScale = d3.scale.linear()
		.domain([0,totalDeath])
		.range(['25px','55px']);
	$('#female_icon').css('width',imgScale(cal_DeathRateTotal(data)));
	/*
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmax])
		.range([500, 0])
		//這次顏色都要用尺度來算
	*/
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#ff5cbd','#ff42b3','#ff29a8','#ff0f9e','#f50092']);

		//.range(['#ffff56','#ffa500','#ff4d4d','#ff0000']);
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = data.length;
	var barHeight = (h - padding * 6) / (data.length*4) - barMargin;
	var errorValueHeight = barHeight/2;
	var svg = d3.select('.tw_bar_female');
	var delay = function(d, i) { return i * 50; };
	svg.selectAll('g .bar').data(data)
        .on('mouseover', function(d){
			tip.show(d);
		 })
	.on('mouseout', function(d){
			tip.hide(d);
		})
			   .transition().duration(750)
        		   .delay(delay)
			   .attr({	
				'id': function(d){ return d['年齡別']+'_b';},
				'y': function(d, i){return yScale(i)+60}, 
				'x': function(d){return 50}, 
				'height': barHeight,
				'width':function(d){
					return xScale((+d['死亡數'] || +d['死亡率'] || 0));
				}, 
				'fill': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color); //利用尺度來算出顏色
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'data-color': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color);
					//return Map[d['縣市']]((+d['死亡數'] || +d['死亡率'] || 0));
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'title': function(d){
					return 'Name : ' + d['年齡別'];
				}
			}).call(tip);


	

	svg.selectAll('.age').data(data).transition().duration(750)
        .delay(delay) 
	.text(function(d){ return d['年齡別']}) 
	.attr({
		'y': function(d, i){return yScale(i) + barHeight/2+65}, //和上面相同，算出X、Y的位置
		'x': function(d){return 20},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	});
	

	svg.select(".axis_text").transition().duration(750)
        .delay(delay)     // text label for the x axis
        .text("年齡別")
        .attr("x", 0 )
        .attr("y", padding +15)
	.attr("fill","SlateGray");

}

function updateBar_male(data, totalDeath){
		var w = $('.gender-section').width()/2-140, h = $('.gender-section').height()*(3/4), padding = 30, barMargin = 2;
		//定義寬高,padding等等值

	$('.male_num').text(cal_DeathRateTotal(data))
	var Xmax = d3.max(data, function(d){return  +d['死亡率']}),
	Xmin = d3.min(data, function(d){return +d['死亡率'] });
	//取得X軸的最大值

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, data.length]) //傳入的值是原始資料的最小及最大值
		.range([0, h/2]); 
		//輸出的範圍是左邊的padd距離，到右邊的padding

	var xScale = d3.scale.linear()
		.domain([0, Xmax])
		.range([20, w - padding]);
	
	var imgScale = d3.scale.linear()
		.domain([0,totalDeath])
		.range(['25px','55px']);
	$('#male_icon').css('width',imgScale(cal_DeathRateTotal(data)));
	/*
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmax])
		.range([500, 0])
		//這次顏色都要用尺度來算
	*/
	var gap = (Xmax - Xmin) / 4;
	var colorScale = d3.scale.linear()
		.domain([Xmin, Xmin+gap, Xmin+2*gap, Xmin+3*gap,Xmax])
		.range(['#80e2e2','#6cdddd','#57d9d9','#42d4d4','#2fcece']);
	

		//.range(['#ffff56','#ffa500','#ff4d4d','#ff0000']);
	window.Xmax = Xmax;
	window.Xmin = Xmin;
	window.dataLength = data.length;
	var barHeight = (h - padding * 6) / (data.length*4) - barMargin;
	var errorValueHeight = barHeight/2;
	var svg = d3.select('.tw_bar_male');
	var delay = function(d, i) { return i * 50; };
	svg.selectAll('g .bar').data(data)
        .on('mouseover', function(d){
			tip.show(d);
		 })
	.on('mouseout', function(d){
			tip.hide(d);
		})
			   .transition().duration(750)
        		   .delay(delay)
			   .attr({	
				'id': function(d){ return d['年齡別']+'_b';},
				'y': function(d, i){return yScale(i)+60}, 
				'x': function(d){return w+60-xScale(d['死亡率'])}, 
				'height': barHeight,
				'width':function(d){
					return xScale((+d['死亡數'] || +d['死亡率'] || 0));
				}, 
				'fill': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color); //利用尺度來算出顏色
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'data-color': function(d){
					//var color = 0.2 + colorScale((+d['死亡數'] || +d['死亡率'] || 0)) * 0.001;
					//return  d3.hsl(200 ,color, color);
					//return Map[d['縣市']]((+d['死亡數'] || +d['死亡率'] || 0));
					return colorScale((+d['死亡數'] || +d['死亡率'] || 0));
				},
				'title': function(d){
					return 'Name : ' + d['年齡別'];
				}
			}).call(tip);
}

function sortBar(method){
	var xScale = d3.scale.linear()
		.domain([window.Xmin, window.Xmax])
		.range([window.padding-20, window.w - window.padding])

	var yScale = d3.scale.linear() //產生一個屬於Y軸的線性尺度
		.domain([0, window.dataLength]) //傳入的值是原始資料的最小及最大值
		.range([window.padding, window.h - window.padding]) 
		//輸出的範圍是左邊的padd距離，到右邊的padding

	var sortItems;
	if(method == '0'){
	sortItems = function (a, b) {
       	  	return d3.descending((a['死亡數'] || a['死亡率'] || 0) , (b['死亡數'] || b['死亡率'] || 0));
   		};
	}
	else if(method == '1'){
	sortItems = function (a, b) {
       	  	return d3.ascending((a['orders'] ) , (b['orders']));
   		};
	}
	var svg = d3.select('#barChart');
	var delay = function(d, i) { return i * 50; };
    svg.selectAll("g .bar")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+15;
    });	
    svg.selectAll("g .errorValue")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+25;
    });
    svg.selectAll("g .errorValue-start")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+22;
    });
    svg.selectAll("g .errorValue-end")
        .sort(sortItems)
        .transition()
        .delay(delay)
        .duration(750)
        .attr("y", function (d, i) {
        return yScale(i)+22;
    });
    svg.selectAll('.city').sort(sortItems).transition().duration(750)
        .delay(delay) 
	.text(function(d){ return d['縣市']}) 
	.attr({
		'y': function(d, i){return yScale(i) + window.barHeight/2+20}, //和上面相同，算出X、Y的位置
		'x': function(d){return 30},
		'fill': 'SlateGray', 
		'text-anchor': 'middle',
		'font-size': '14px' 
	}); 
    /*
    svg.selectAll('.data_text').sort(sortItems).transition().duration(750)
        .delay(delay)
	.text(function(d){ return (d['死亡數'] || d['死亡率'] || 0)}) //將值寫到SVG上
	.attr({
		'y': function(d, i){return yScale(i) + window.barHeight/2}, //和上面相同，算出X、Y的位置
		'x': function(d){ return xScale((+d['死亡數'] || +d['死亡率'] || 0)) +70;},
		'fill': 'black', 
		'text-anchor': 'middle',
		'font-size': '10px'
	});
   */

}
