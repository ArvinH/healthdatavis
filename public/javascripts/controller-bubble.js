(function(){
	$.getJSON('http://140.116.247.114:3000/data/bubble-initial', function(json, textStatus){
		$('#timeline-sidebar').append('<div  style="height:calc(30% + 45px)">'
    			+'<div class="content">'
      			+'<div class="header"></div>'
    			+'</div>'
  				+'</div>');
		for(data in json[0]){
			$('#timeline-sidebar').append('<div class="item">'
    			+'<div class="content">'
      			+'<div class="header">'+json[0][data]['西元年']+'</div>'
    			+'</div>'
  				+'</div>');
		}	
		for( data in json[1]){
			if( json[1][data]['死因別'] === '所有死因'){
		   		$('.reason').append('<div class="item selected" data-value='+json[1][data]['死因別']+'>'+json[1][data]['死因別']+'</div>');
			}
			else{
		   		$('.reason').append('<div class="item" data-value='+json[1][data]['死因別']+'>'+json[1][data]['死因別']+'</div>');
			}
	  	}
		$('#timeline-sidebar').append('<div  style="height:calc(60%)">'
    			+'<div class="content">'
      			+'<div class="header"></div>'
    			+'</div>'
  				+'</div>');
		$('#timeline-sidebar .item').first().css('font-size','25px');
		function scrollAnimate(){
			$('#timeline-sidebar .item').each(function(){
					if($(this).offset().top < ($('.arrow-left').offset().top + 20) && $(this).offset().top >= ($('.arrow-left').offset().top - 20)){
								$(this).animate({
									fontSize: "25px"
								}, 50);		
								sendRequest();
					}
					else{
						$(this).animate({
							fontSize: "14px"
						}, 50);
					}
				});
		}
		$('#timeline-sidebar').on('scroll',scrollAnimate);
		$('#timeline-sidebar .item').on('click',function(){
				$('#timeline-sidebar .item').animate({
					fontSize: "14px"
				}, 500);
				$('#timeline-sidebar').off('scroll');
				var y = $('.arrow-left').offset().top;
				var thisItem_pos = $(this).offset().top;
				var offset = y - thisItem_pos;
				var finalOffset = $('#timeline-sidebar').scrollTop() - offset;
				$(this).animate({
					fontSize: "25px"
				}, 300);
				$('#timeline-sidebar').animate({
          			scrollTop: finalOffset - 16
        	 	}, 1000, function(){
					sendRequest()
					$('#timeline-sidebar').on('scroll',scrollAnimate);
				});
		});

	});
	
	$('.dropdown').dropdown({
	      onChange: function(value, text) {
			sendRequest();
	    }
	});

 	
   
})();
function sendRequest(){
	var reason = $('div.reasonDrop').find('.selected').text();
	        var year;
			$('#timeline-sidebar .item').each(function(){
					if($(this).offset().top < ($('.arrow-left').offset().top + 20) && $(this).offset().top >= ($('.arrow-left').offset().top - 20)){
						year = $(this).text();				
					}
				});
		var city = $('.city-name').attr('id').split('_')[0];

		var queryData = {
				reason: reason,
				year: year,
				city: city
		};
		if (reason != "" && year != ""){
	    		$.ajax({type:'POST', url:'/data/bubble-query/bar',data: queryData, success: onDataSubmitted_Bar});
	    		$.ajax({type:'POST', url:'/data/bubble-query/map',data: queryData, success: onDataSubmitted_Map});
		}

  }

function onDataSubmitted_Bar(returnData){
		var totalDeath = cal_DeathRateTotal(returnData);
		updateBar_female(divideData(returnData,2)[0],totalDeath);
		updateBar_male(divideData(returnData,2)[1],totalDeath);

   }

function onDataSubmitted_Map(returnData){
		
		updateMap(returnData);

   }
