(function(){
var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([65, 100])
	  .html(function(d) {
		return "<strong>" + d.properties.name + "<br>死亡率:</strong> <span style='color:red'>" + d.properties.value + "</span>";
	  });
	
var window_width = $(window).width();
var window_height = $(window).height();
var mapScale;
if(window_width > 900){
	mapScale = 8000;
}
else{
	mapScale = 8000;
}

var cityNameMap = function(cityName){
	if(cityName === '臺北市') return 'TAIPEI CITY';
	else if(cityName === '基隆市') return 'KEELUNG CITY';
	else if(cityName === '新北市') return 'NEW TAIPEI CITY';
	else if(cityName === '桃園市') return 'TAOYUAN CITY';
	else if(cityName === '新竹市') return 'HSINCHU CITY';
	else if(cityName === '新竹縣') return 'HSINCHU COUNTY';
	else if(cityName === '苗栗縣') return 'MIAOLI COUNTY';
	else if(cityName === '臺中市') return 'TAICHUNG CITY';
	else if(cityName === '彰化縣') return 'CHANGHUA COUNTY';
	else if(cityName === '雲林縣') return 'YUNLIN COUNTY';
	else if(cityName === '南投縣') return 'NANTOU COUNTY';
	else if(cityName === '嘉義市') return 'CHIAYI CITY';
	else if(cityName === '嘉義縣') return 'CHIAYI COUNTY';
	else if(cityName === '臺南市') return 'TAINAN CITY';
	else if(cityName === '高雄市') return 'KAOHSIUNG CITY';
	else if(cityName === '屏東縣') return 'PINGTUNG COUNTY';
	else if(cityName === '花蓮縣') return 'HUALIEN COUNTY';
	else if(cityName === '臺東縣') return 'TAITUNG COUNTY';
	else if(cityName === '宜蘭縣') return 'YILAN COUNTY';
	else if(cityName === '金門縣') return 'KINMEN COUNTY';
	else if(cityName === '澎湖縣') return 'PENGHU COUNTY';
	
};
	


d3.json("data/twCounty2010merge.topo.json", function (error, data) {
	$.ajax({type:'POST', url:'/data/bubble-query/map',data: {reason:'所有死因', year:'1981'}, success: function(Data)  {
		var population = new Array();
		for(var i = 0, len = Data.length; i < len; i+=1) {
			if(Data[i]['死亡率'] != 0){
				population[Data[i]['縣市']] = Data[i]['死亡率'];
			}
			else if (!Data[i]['死亡率']){
				population[Data[i]['縣市']] = '0 ';
			}
		}
		
  		topo = topojson.feature(data, data.objects.layer1);
		prj = d3.geo.mercator().center([122.079531, 23.978567]).scale(mapScale);
		path = d3.geo.path().projection(prj);					
		var DataMax = d3.max(Data, function(d){return (+d['死亡率'] || 0)});
        var DataMin = d3.min(Data, function(d){return (+d['死亡率'] || 0)});
		var delay = function(d, i) {return i * 50; };			
		for(var i = 0, len = topo.features.length; i < len; i+=1) {
			topo.features[i].properties.value = population[topo.features[i].properties.name]
		}
		var gap = (DataMax - DataMin) / 4;
		var colorScale = d3.scale.linear()
			.domain([DataMin, DataMin+gap, DataMin+2*gap, DataMin+3*gap ,DataMax])
			.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);
		var svg = d3.select("#tw_map").append("svg").attr({"width":500,"height":600});
		locks = svg.selectAll("path")
				.data(topo.features).enter()
				.append("path")
				.on('mouseover',function(d){ 
					$(this).attr("opacity","0.5"); tip.show(d);
                 			$(this).attr("fill",$(this).attr("data-color"));
				   	$('#'+d.properties.name+'_b').attr("fill","red");	
				})
				.on('mouseout',function(d){ 
					$(this).attr("opacity","1"); tip.hide(d);
				   	$('#'+d.properties.name+'_b').attr("fill",$('#'+d.properties.name+'_b').attr("data-color"));	
				})
				.on('click', function(d){
    					$('.city-name').animate({opacity: "0"}, 500, function() {
        				$(this)
          				.hide()
          				.text(cityNameMap(d.properties.name))
						.attr('id',d.properties.name+'_title')
          				.show()
          				.animate({opacity: "100"}, 1000); 
						sendRequest();
    				});
				})
				.attr("fill",function(d){ 
					return colorScale(d.properties.value);
				 })
				.attr("data-color", function(d){ 
					return colorScale(d.properties.value);
				})
				.attr("d", path)
				.attr("cursor","pointer")
				.attr("class",function(d){ return d.properties.name+' citymap';});
		locks.call(tip);
		$('#tw_map').append('<div class="notes">死亡率單位(人/每十萬人口)</div>')
		
	}});
});
})();				
function updateMap(Data){
$('.d3-tip').remove();
var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([65, 100])
	  .html(function(d) {
		var text;
		if('死亡數' in Data[0]){
		   return "<strong>" + d.properties.name + "<br>死亡數:</strong> <span style='color:red'>" + (d.properties.value || 0)+ "人</span>";
		}
		else if ('死亡率' in Data[0]){
		   return "<strong>" + d.properties.name + "<br>死亡率:</strong> <span style='color:red'>" + (d.properties.value || 0)+ "</span>";
		}
	  });
	
var window_width = $(window).width();
var window_height = $(window).height();
var mapScale;
if(window_width > 900){
	mapScale = 8000;
}
else{
	mapScale = 8000;
}
var cityNameMap = function(cityName){
	if(cityName === '臺北市') return 'TAIPEI CITY';
	else if(cityName === '基隆市') return 'KEELUNG CITY';
	else if(cityName === '新北市') return 'NEW TAIPEI CITY';
	else if(cityName === '桃園市') return 'TAOYUAN CITY';
	else if(cityName === '新竹市') return 'HSINCHU CITY';
	else if(cityName === '新竹縣') return 'HSINCHU COUNTY';
	else if(cityName === '苗栗縣') return 'MIAOLI COUNTY';
	else if(cityName === '臺中市') return 'TAICHUNG CITY';
	else if(cityName === '彰化縣') return 'CHANGHUA COUNTY';
	else if(cityName === '雲林縣') return 'YUNLIN COUNTY';
	else if(cityName === '南投縣') return 'NANTOU COUNTY';
	else if(cityName === '嘉義縣') return 'CHIAYI CITY';
	else if(cityName === '嘉義市') return 'CHIAYI COUNTY';
	else if(cityName === '臺南市') return 'TAINAN CITY';
	else if(cityName === '高雄市') return 'KAOHSIUNG CITY';
	else if(cityName === '屏東縣') return 'PINGTUNG COUNTY';
	else if(cityName === '花蓮縣') return 'HUALIEN COUNTY';
	else if(cityName === '臺東縣') return 'TAITUNG COUNTY';
	else if(cityName === '宜蘭縣') return 'YILAN COUNTY';
	else if(cityName === '金門縣') return 'KINMEN COUNTY';
	else if(cityName === '澎湖縣') return 'PENGHU COUNTY';
	
};
d3.json("data/twCounty2010merge.topo.json", function (error, data) {
		var population = new Array();
		for(var i = 0, len = Data.length; i < len; i+=1) {
			if(Data[i]['死亡率'] != 0){
				population[Data[i]['縣市']] = Data[i]['死亡率'];
			}
			else if (!Data[i]['死亡率']){
				population[Data[i]['縣市']] = '0 ';
			}
		}
  		topo = topojson.feature(data, data.objects.layer1);
		prj = d3.geo.mercator().center([122.079531, 23.978567]).scale(mapScale);
		path = d3.geo.path().projection(prj);					
		var DataMax = d3.max(Data, function(d){return (+d['死亡率'] || 0)});
                var DataMin = d3.min(Data, function(d){return (+d['死亡率'] || 0)});
		var delay = function(d, i) {return i * 50; };
		for(var i = 0, len = topo.features.length; i < len; i+=1) {
			topo.features[i].properties.value = population[topo.features[i].properties.name]
		}
		var gap = (DataMax - DataMin) / 4;
		var colorScale = d3.scale.linear()
			.domain([DataMin, DataMin+gap, DataMin+2*gap, DataMin+3*gap ,DataMax])
			.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);
		var svg = d3.select("#tw_map");
		locks = svg.selectAll("path")
				.data(topo.features)
				.on('mouseover',function(d){ 
					$(this).attr("opacity","0.5"); tip.show(d);
                 			$(this).attr("fill",$(this).attr("data-color"));
				   	$('#'+d.properties.name+'_b').attr("fill","red");	
				})
				.on('mouseout',function(d){ 
					$(this).attr("opacity","1"); tip.hide(d);
				   	$('#'+d.properties.name+'_b').attr("fill",$('#'+d.properties.name+'_b').attr("data-color"));	
				})
				.on('click', function(d){
    					$('.city-name').animate({opacity: "0"}, 500, function() {
        				$(this)
          				.hide()
          				.text(cityNameMap(d.properties.name))
						.attr('id',d.properties.name+'_title')
          				.show()
          				.animate({opacity: "100"}, 1000); 
						sendRequest();
    				});
				})
				.transition().duration(750).delay(delay)
				.attr("fill",function(d){ 
					return colorScale(d.properties.value);
 				})
				.attr("data-color", function(d){ 
					return colorScale(d.properties.value);
				})
				.attr("d", path)
				.attr("cursor", "pointer")
				.attr("class",function(d){ return d.properties.name+' citymap'});
		locks.call(tip);
});
}
