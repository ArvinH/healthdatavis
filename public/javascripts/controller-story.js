(function(){
	$.getJSON('http://140.116.247.114:3000/data/initial', function(json, textStatus) {
	/*optional stuff to do after success */
	var reasonArray = [];
	var yearArray = [];
	for( data in json[0]){
		for( key in json[0][data]){
		    $('.year').append('<div class="item" data-value='+json[0][data][key]+'>'+json[0][data][key]+'</div>');
		}
		
    }
	
 	  for( data in json[1]){
		for( key in json[1][data]){
		    $('.reason').append('<div class="item" data-value='+json[1][data][key]+'>'+json[1][data][key]+'</div>');
		}
	  }
	    $('.dropdown').dropdown({
	      onChange: function(value, text) {
		$('.sortmethod').addClass('disabled');
		$('.sortmethod')[0].innerHTML = '<i class="icon sort"></i>依死亡率排序'; 
		/*
		if(text.indexOf("排序") > -1){
	        	var sortmethod = $('div.menu.sortmethod').find('.selected').text();
			if(sortmethod == '依死亡數(率)排序'){
				sortBar();
			}
			else if (sortmethod == '依縣市排序'){

			}
		}	
		*/
	        var reason = $('div.menu.reason').find('.selected').text();
	        var gender = $('div.menu.gender').find('.selected').text();
	        var year = $('div.menu.year').find('.selected').text();
	        //var age = $('div.menu.age').find('.selected').text();
	        //var display = $('div.menu.display').find('.selected').text();
		if(gender == '男'){gender=1;}
		else if (gender == '女'){gender=2;}
		else if (gender == '男女合計'){ gender=3;}
		if(reason == ''){
		    reason = '所有死因';
		}if(gender == ''){
		    gender = 3;
		}if(year == ''){
		    year = '2011-2013';
		}


		var queryData = {
				reason: reason,
				gender: gender,
				year: year
		};
		if (reason != "" && gender != "" && year != ""){
	    		$.ajax({type:'POST', url:'/data/query',data: queryData, success: onDataSubmitted});
		}

	        }

	    });
		$('.showHL').on('click',null,function(){
			if( $('.showHLicon').hasClass('on') ){
				$('.showHLicon').removeClass('on').addClass('off');
			}
			else{
				$('.showHLicon').removeClass('off').addClass('on');
			}
		});

	    $('.sortmethod').on('click', null, function(){
		if( $($(this)[0]).text() == '依死亡率排序' ){
			$(this)[0].innerHTML = '<i class="icon sort"></i>依縣市排序'; 
			sortBar(0);
		}
		else {
			$(this)[0].innerHTML = '<i class="icon sort"></i>依死亡率排序'; 
			sortBar(1);
		}
	    });
	    function onDataSubmitted(returnData){
			/*
			d3.selectAll('path').each(function(){
   			 	d3.select('.citymap').on('mouseover', null);  //this should remove the event listener
			 });
*/
			updateBar(returnData);
			updateMap(returnData);
		$('.sortmethod').innerHTML = '<i class="icon sort"></i>依死亡率排序';
		setTimeout(function(){$('.sortmethod').removeClass('disabled');},1500);
  	    }
	   
	});
	
	$('.story-inbox').on('scroll',scrollCallback);	
	listController();
})();

function scrollCallback(){
	var currentScrollTop = $('#story').height()/2;
	var story1ScrollTop = $('#story-1').offset().top;
	var story2ScrollTop = $('#story-2').offset().top;
	var story3ScrollTop = $('#story-3').offset().top;
	var story4ScrollTop = $('#story-4').offset().top; 
	if(story1ScrollTop > currentScrollTop - 10 && story1ScrollTop < currentScrollTop + 15){
		$('div.reasonDrop').find("[data-value='糖尿病']").trigger("click");
		$('div.yearDrop').find("[data-value='1999-2001']").trigger("click");
	}	
	if(story2ScrollTop > currentScrollTop - 10 && story2ScrollTop < currentScrollTop + 15){
		$('div.reasonDrop').find("[data-value='糖尿病']").trigger("click");
		$('div.yearDrop').find("[data-value='2002-2004']").trigger("click");
	}
	if(story3ScrollTop > currentScrollTop - 10 && story3ScrollTop < currentScrollTop + 15){
		$('div.reasonDrop').find("[data-value='自殺']").trigger("click");
		$('div.yearDrop').find("[data-value='2005-2007']").trigger("click");
	}
	if(story4ScrollTop > currentScrollTop - 10 && story4ScrollTop < currentScrollTop + 15){
		$('div.reasonDrop').find("[data-value='糖尿病']").trigger("click");
		$('div.yearDrop').find("[data-value='2011-2013']").trigger("click");
	}
}

function listController(){
	$('.slide-list div').each(function(){
		$(this).on('click',function(){
			var storys = $(this).attr('data-value');
			var scrollOffset = $('#story-'+storys).offset().top;
			var targetScrollTop = $('#story').height()/2;
			var offset = targetScrollTop - scrollOffset;
		    var currentScrollTop = $('.story-inbox').scrollTop();	
			$('.story-inbox').animate({scrollTop: currentScrollTop - offset },1000);
		});	
	});
}
