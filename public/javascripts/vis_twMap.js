(function(){
var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([65, 100])
	  .html(function(d) {
		return "<strong>" + d.properties.name + "<br>死亡率:</strong> <span style='color:red'>" + d.properties.value + "</span>";
	  });



	
var window_width = $(window).width();
var window_height = $(window).height();
var mapScale;
if(window_width > 900){
	mapScale = 10000;
}
else{
	mapScale = 10000;
}

d3.json("data/twCounty2010merge.topo.json", function (error, data) {
	$.ajax({type:'POST', url:'/data/query',data: {reason:'所有死因', gender:3,year:'2011-2013'}, success: function(Data)  {
		var population = new Array();
		for(var i = 0, len = Data.length; i < len; i+=1) {
			if(Data[i]['死亡率'] != 0){
				population[Data[i]['縣市']] = Data[i]['死亡率'] +' '+ Data[i]['信賴區間'];
			}
			else if (!Data[i]['死亡率']){
				population[Data[i]['縣市']] = '0 '+ Data[i]['信賴區間'];
			}
		}
		
  		topo = topojson.feature(data, data.objects.layer1);
		prj = d3.geo.mercator().center([122.079531, 23.978567]).scale(mapScale);
		path = d3.geo.path().projection(prj);					
		var DataMax = d3.max(Data, function(d){return (+d['死亡率'] || 0)});
                var DataMin = d3.min(Data, function(d){return (+d['死亡率'] || 0)});
		var delay = function(d, i) {return i * 50; };			
		for(var i = 0, len = topo.features.length; i < len; i+=1) {
			topo.features[i].properties.value = population[topo.features[i].properties.name]
		}
		var gap = (DataMax - DataMin) / 4;
		var colorScale = d3.scale.linear()
			.domain([DataMin, DataMin+gap, DataMin+2*gap, DataMin+3*gap ,DataMax])
			.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);
		var svg = d3.select("#tw_map").append("svg").attr({"width":600,"height":650});
		locks = svg.selectAll("path")
				.data(topo.features).enter()
				.append("path")
				.on('mouseover',function(d){ 
					$(this).attr("opacity","0.5"); tip.show(d);
                 			$(this).attr("fill",$(this).attr("data-color"));
				   	$('#'+d.properties.name+'_b').attr("fill","red");	
				})
				.on('mouseout',function(d){ 
					$(this).attr("opacity","1"); tip.hide(d);
				   	$('#'+d.properties.name+'_b').attr("fill",$('#'+d.properties.name+'_b').attr("data-color"));	
				})
				.attr("fill",function(d){ 
					return colorScale(d.properties.value.split(' ')[0]);
				 })
				.attr("data-color", function(d){ 
					return colorScale(d.properties.value.split(' ')[0]);
				})
				.attr("d", path)
				.attr("class",function(d){ return d.properties.name+' citymap';});
		locks.call(tip);
		initialBar(Data);
		$('#tw_map').append('<div class="notes">死亡率單位(人/每十萬人口)</div>')
		
	}});
});
})();				
function updateMap(Data){
var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([65, 100])
	  .html(function(d) {
		var text;
		if('死亡數' in Data[0]){
		   return "<strong>" + d.properties.name + "<br>死亡數:</strong> <span style='color:red'>" + (d.properties.value || 0)+ "人</span>";
		}
		else if ('死亡率' in Data[0]){
		   return "<strong>" + d.properties.name + "<br>死亡率:</strong> <span style='color:red'>" + (d.properties.value || 0)+ "</span>";
		}
	  });
	
var window_width = $(window).width();
var window_height = $(window).height();
var mapScale;
if(window_width > 900){
	mapScale = 10000;
}
else{
	mapScale = 10000;
}

d3.json("data/twCounty2010merge.topo.json", function (error, data) {
		var population = new Array();
		for(var i = 0, len = Data.length; i < len; i+=1) {
			if(Data[i]['死亡率'] != 0){
				population[Data[i]['縣市']] = Data[i]['死亡率'] +' '+ Data[i]['信賴區間'];
			}
			else if (!Data[i]['死亡率']){
				population[Data[i]['縣市']] = '0 '+ Data[i]['信賴區間'];
			}
		}
  		topo = topojson.feature(data, data.objects.layer1);
		prj = d3.geo.mercator().center([122.079531, 23.978567]).scale(mapScale);
		path = d3.geo.path().projection(prj);					
		var DataMax = d3.max(Data, function(d){return (+d['死亡率'] || 0)});
                var DataMin = d3.min(Data, function(d){return (+d['死亡率'] || 0)});
		var delay = function(d, i) {return i * 50; };
		for(var i = 0, len = topo.features.length; i < len; i+=1) {
			topo.features[i].properties.value = population[topo.features[i].properties.name]
		}
		var gap = (DataMax - DataMin) / 4;
		var colorScale = d3.scale.linear()
			.domain([DataMin, DataMin+gap, DataMin+2*gap, DataMin+3*gap ,DataMax])
			.range(['#CFC99C','#9E9237','#766D29','#4F491C','#3B3615']);
		var svg = d3.select("#tw_map");
		locks = svg.selectAll("path")
				.data(topo.features)
				.on('mouseover',function(d){ 
					$(this).attr("opacity","0.5"); tip.show(d);
                 			$(this).attr("fill",$(this).attr("data-color"));
				   	$('#'+d.properties.name+'_b').attr("fill","red");	
				})
				.on('mouseout',function(d){ 
					$(this).attr("opacity","1"); tip.hide(d);
				   	$('#'+d.properties.name+'_b').attr("fill",$('#'+d.properties.name+'_b').attr("data-color"));	
				})
				.transition().duration(750).delay(delay)
				.attr("fill",function(d){ 
					return colorScale(d.properties.value.split(' ')[0]);
 				})
				.attr("data-color", function(d){ 
					return colorScale(d.properties.value.split(' ')[0]);
				})
				.attr("d", path)
				.attr("class",function(d){ return d.properties.name+' citymap'});
		locks.call(tip);
});
}
